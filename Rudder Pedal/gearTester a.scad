$fn=100;
use <AnotherGearGenerator/gears.scad>
//use <gears.scad>

//projection(cut=false)
//rotate([90,0,0])

module bevelGear(){
  nbTeeth = 30;
  //rotate([0,0,360/(2*nbTeeth)])
  bevel_gear(modul=1, tooth_number=nbTeeth, partial_cone_angle=45, tooth_width=5, bore=4, pressure_angle=20, helix_angle=0);
}
//bevelGear();
/*
ECHO: "Part Cone Diameter at the Cone Base = ", 30
ECHO: "Bevel Gear Height = ", 3.70249
*/

module coneTop(dd=22.3730+1.2231){
  //mirror([0,0,1])
    cylinder(h=22,d=dd);
}
echo("top diameter=",22.3730+1.2231);

//intersection(){
bevel_gear(modul=1, tooth_number=32, partial_cone_angle=45, tooth_width=5, bore=4, pressure_angle=20, helix_angle=0);

//coneTop();
//}
module bevelGearPair(){
    bevel_gear_pair(modul=1, gear_teeth=30, pinion_teeth=30, axis_angle=90,
                    tooth_width=5, gear_bore=4, pinion_bore=8,
                    pressure_angle = 20, helix_angle=0, together_built=false);
}
/*
projection(true)
rotate([90,0,0])
bevel_gear(modul=1, tooth_number=30, partial_cone_angle=45, tooth_width=5, bore=4, pressure_angle=20, helix_angle=0);
*/
/*
ECHO: "Cone Angle Gear = ", 45
ECHO: "Cone Angle Pinion = ", 45
ECHO: "Gear Height = ", 15.8019
ECHO: "Pinion Height = ", 15.8019
ECHO: "Part Cone Diameter at the Cone Base = ", 30
ECHO: "Bevel Gear Height = ", 3.70249
ECHO: "Part Cone Diameter at the Cone Base = ", 30
ECHO: "Bevel Gear Height = ", 3.70249
*/