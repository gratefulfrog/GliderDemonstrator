$fn=100;

use <AnotherGearGenerator/gears_original.scad>

//////////////////// TARGETS //////////////////////

/*
 * 2024 02 16a :
 * printed :
 * 3x gears, holes are too small
 * 2x pedals, axels are fucked up:
   * hole for pot is too small
   * gear side does nto fit gear hole or bearing
 * so I fixed the axes and gears 0 and 1, I think...
 *
 * 2024 02 16b :
 * printed gears 0 & 2, the sxis (failed, too pencilish)
 * still wrong dimensions for both the gear and pot
 * tried again..
 *
 * 2024 02 16c:
 * better, the parts fit together, a little too  much slop between the gear and the axis,
 * reinforcement need on the pot side of the axis
 * printing the pedal in halves sucks, print with the gear side facing downwards,
 * need to reprint the right pedal and the gear0
 *
 * 2024 02 17 D
 * some dimensions screwed up and fixed, I hope
 * trying to print on smooth PEI with 3DLAC, bed at 90C instead of 100C
 * first attempt : 3 gears, 100% infill, will wait for Fully cooled bed before removing
 * RESULTS :
 * really good,
 * everything seems to fit, especially the pot!
 * but I broke an axis by forcing...
 * reprinting the pedals + g0 and G1, using the axes and gears infill at 100% otherwise 18% gyroid
 *
 * 2024 02 18
 * D last print was partially ok,
   * the pedal itself was ok, but a little bad stuff on the outside edge of one
   * the pot side axis is perfect!
   * the gear side axis is too thin 
   * and/or 
   * gear hole is too big!
   * how to fix this?
   * worked on the gear hole... let's see if the new hole fits the old axis?
  * version E
    * will try a z offset of +0.01 to see if it will be easier to remove the prints from the bed
    * G0 and G2 version E, fit pedals version D,
    * it seemed easier to remove with +0.01, next time let's try +0.02.
    * +0.02 is perfect!!
    * why not try +0.03 just for fun?
      * +0.03 is really good, even better than 0.02 !
    * IT ALL FITS TOGETHER !
    * but : 
      * the bearing cups need to be 180° not what they are,
      * the front needs to be as high as the back!
 */

// there is something wrong with the computations of the 2 bevel gears
// with regard to a single bevel gear... The heights are not the same!
// bidoulage fix in gears.scad... then removed! 
// the issue needs to be addressed properly

//projection(cut=true)
//getGear(0);
//getGear(1);
//translate([0,0,-0.5*pedalWidth])
//getGear(2);
//fullPedalAsselmbly(false,false);
//intersection(){
getSup(front=true,rear=true,base=true);
assemblyPositioned(gears=true);
//}
//translate([0,0,-pedalWidth])
//axis();
//translate([0,0,-pedalWidth])
//axis();
//axisRaw(true);
//thrustBearingEmulator();
////////////////////// end of targets
 
 
cD = 34;//45;
cH = 70 -1.235;

/// so we can get the space needed by the gears
DO_GEAR_HULL = false;
// to control pedal generation
DO_PEDAL = true;

/// DXF stuff
outlineFileName      = "DXF/rudderPedalMaker f profile full assembly.dxf";
outlineLayerName     = "mirrored_offset_outline";
blockLayerName       = "building blocks";
coverLayer           = "building blocks cover";
rearFinishBlockLayer = "rear finishing block";
baseLayer            = "building blocks base";
baseBaseLayer        = "base base";
printEnablerLayer    = "print enablers";
thrustBearingBaseBaseLayer = "thrust bearing base base";
washerBaseBaseLayer        = "washer base base";

gearSlop           = 1;
bearingSlop        = 0.2;
thrustBearingDepth = 3.5 + bearingSlop;
thrustBearingDia   = 8 + bearingSlop;

extrudeAlpha=135;

coverZ = 2;
blockZ = max(33.2+1,cD*sin(extrudeAlpha/2));
baseBaseZ = 15;

//echo("Block Z", blockZ);  //34.2
//echo("baseBase Z", baseBaseZ);  //15

thrustBearingBaseBaseDepth = thrustBearingDepth- gearSlop - coverZ;
//echo("thrust Bearing base base depth", thrustBearingBaseBaseDepth);  //0.7
washerBaseDepth = baseBaseZ/3;

module thrustBearingEmulator(){
  difference(){
    cylinder(h=thrustBearingDepth-bearingSlop,d=thrustBearingDia-bearingSlop);
    cylinder(h=thrustBearingDepth-bearingSlop,d=boltD);
  }
}


module supAxisCut(){
  rotate([90,0,0])
    cylinder(h=90,d=8.5,center=true);
}
//supAxisCut();

module supRearRawRaw(){
  mirror([1,0,0]){
    rotate([90,0,0])
      rotate([0,0,-0.5*(extrudeAlpha-45)])
        rotate_extrude(angle=extrudeAlpha)
          import(outlineFileName,layer=outlineLayerName);
    linear_extrude(blockZ,center=true)
      import(outlineFileName,layer=blockLayerName);
    translate([0,0,-coverZ+blockZ/2])
      linear_extrude(coverZ,center=false)
        import(outlineFileName,layer=coverLayer);
    translate([0,0,-(coverZ+blockZ/2)])
      linear_extrude(coverZ,center=false)
        import(outlineFileName,layer=coverLayer);
    translate([0,0,-(coverZ+blockZ/2)])
      linear_extrude(coverZ,center=false)
        import(outlineFileName,layer=baseLayer);
    printEnablers();
mirror([0,0,1])
printEnablers();
  }
  
}
module frontFinishingBlock(){
  translate([0,0,0])
      linear_extrude(blockZ/2,center=false)
        import(outlineFileName,layer=rearFinishBlockLayer);
}

module printEnablers(){
  translate([0,0,-blockZ/2])
      linear_extrude(blockZ*.5,center=false)
        import(outlineFileName,layer=printEnablerLayer);
}
//printEnablers();

module supRearRaw(){
  cLen = 90;
  cDia = 14.5;
  cuTransX = -22.5+cDia/2;
  cuTransZ = (blockZ/2)-cDia/2;
  difference(){
    supRearRawRaw();
    // removed the rounded edge which sucked
    /*translate([cuTransX,0,cuTransZ])
        difference(){
          translate([-cDia/2,-cLen/2,0])
            cube( [cDia/2,cLen,cDia/2]);
          rotate([90,0,0])
            cylinder(h=cLen,d=cDia,center=true);
        } */
  }
}

module supFrontRaw(){
  //extrudeAlpha = 360;
  difference(){
    union(){
      rotate([90,0,0])
        rotate([0,0,-0.5*(extrudeAlpha-45)])
          rotate_extrude(angle=extrudeAlpha) /*/2)*/
            import(outlineFileName,layer=outlineLayerName);
      translate([0,0,-blockZ/4])
        linear_extrude(blockZ/2,center=true)
          import(outlineFileName,layer=blockLayerName);
      translate([0,0,-(coverZ+blockZ/2)])
        linear_extrude(coverZ,center=false)
          import(outlineFileName,layer=coverLayer);
      translate([0,0,-(coverZ+blockZ/2)])
        linear_extrude(coverZ,center=false)
          import(outlineFileName,layer=baseLayer);
      frontFinishingBlock();
      printEnablers();
    }
    frontBaseMountingHoleCutters();
  }
}
//supFrontRaw();
module supBaseRaw(){
  difference(){ 
    translate([0,0,-(baseBaseZ+blockZ/2+coverZ)])
      linear_extrude(baseBaseZ,center=false)
        import(outlineFileName,layer=baseBaseLayer);
    translate([0,0,-(thrustBearingBaseBaseDepth+blockZ/2)])
          linear_extrude(thrustBearingBaseBaseDepth,center=false)
            import(outlineFileName,layer=thrustBearingBaseBaseLayer);

    translate([0,0,-(baseBaseZ+blockZ/2)])
          linear_extrude(washerBaseDepth,center=false)
            import(outlineFileName,layer=washerBaseBaseLayer);
    frontBaseMountingHoleCutters();
  }
}

module getSup(front=true,rear=true,base=true){
  difference(){
    union(){
      if(front) supFrontRaw();
      if(rear)  supRearRaw();
      if(base)  supBaseRaw();
    }
    supAxisCut();
  }
}
//getSup(true,false,false);

////////////////////////////////////////////////////

/// computed gear dimensions:
//gearSpace(0);
/*
Bounding box:  
max diameter = 33.2
max height   = 4.71
   Min:  -16.60, -16.60, 0.00
   Max:  16.60, 16.60, 4.71
   Size: 33.20, 33.20, 4.71
*/
//gearSpace(1);
/*axis
Bounding box:
max diameter = 29.3
max height   = 4.95
   Min:  -16.74, -14.65, 0.20
   Max:  -11.79, 14.65, 29.51
   Size: 4.95, 29.30, 29.30
*/
//gearSpace(2);
/*
same as gear zero, thankfully!
Bounding box:
   Min:  -16.60, -16.60, 25.00
   Max:  16.60, 16.60, 29.71
   Size: 33.20, 33.20, 4.71
*/



// dimensions

bearingInnerDiameter = 8;
bearingOuterDiameter = 22;
bearingInnerEpsilon  = 0.3;
bearingOuterEpsilon  = 0.5;
bearingWidth         = 7;
bearingRimWidth      = 1.4;
bearingRimHeight     = 0.4;

bearingSocketwidth  = 6;

potAxisLength   = 15;
potDLength      = 7;
potAxisDiameter = 6;
potDOffSet      = 4.5+0.2;
potAxisEpsilon  = 0.2;

pedalWidth  = 30;
pedalDepth  = 15;
pedalLength = 80;
//pedalXExtender = (bearingOuterDiameter+2*bearingSocketwidth)/2-2;
pedalXExtender = 2*bearingSocketwidth-4;

pedalNeutralPositionAlpha = 60;

//bevelGearHeight = max(3.69231,3.47212); // big gear
bevelGearHeight      = 3.47212; // both big gears
pinonGearHeight      = 3.91558; // smalll vertical pinion
pinionGearTranslateX = -16.7438;
pinionGearTranslateZ = 14.8565;

bearingSocketZ = bearingWidth+bevelGearHeight;
bearingSocketD = bearingOuterDiameter+2.2*bearingSocketwidth;

TOGETHER = true;

nbTeethPedalGear = 32;
nbTeethIdlerGear = 28;
idlerGearBore    = 3.2;
    

module bearing(center=true,maximize=false){
  zTranslation = center ? -bearingWidth/2 : 0;
  translate([0,0,zTranslation])
    if (maximize){
      cylinder(h=bearingWidth,d=bearingOuterDiameter);
    }
    else{
    difference(){
      cylinder(h=bearingWidth,d=bearingOuterDiameter);
      cylinder(h=bearingWidth,d=bearingOuterDiameter-2*bearingRimWidth);
    }
    difference(){//bearingSocketwidth  = 6;
      cylinder(h=bearingWidth,d=bearingInnerDiameter+2*bearingRimWidth);
      cylinder(h=bearingWidth,d=bearingInnerDiameter);
    }
    translate([0,0,bearingRimHeight])
    difference(){
      cylinder(h=bearingWidth-2*bearingRimHeight,d=bearingOuterDiameter);
      cylinder(h=bearingWidth-2*bearingRimHeight,d=bearingInnerDiameter);
    }
  }
}

//bearing(false);
//rotate([90,0,0])
//cylinder(h=bearingWidth,d=bearingOuterDiameter,center=true);

module bearings(extraWidth=false,maxim=false){
  if (extraWidth)
    resize([bearingOuterDiameter,bearingOuterDiameter,bearingSocketZ])
      bearing(false,maxim);
  else
     bearing(false,maxim);
  zTranslation = -(bearingSocketZ) + pedalWidth;
  translate([0,0,zTranslation])
    bearing(false,maxim);
}
//bearings();

module axisRaw(inner=true){
  epSign = inner ? -1 : +0.; // +1.5;
  dia = bearingInnerDiameter + epSign*bearingInnerEpsilon;
  xTransFlatExtra = inner ? -0.2 +.4 : +0.15;
  echo("dia",dia,"xtra",xTransFlatExtra);
  cuDim = [bearingOuterDiameter,
           bearingOuterDiameter,
           bearingSocketZ];
  difference(){
    cylinder(h=pedalWidth,d=dia); //bearingInnerDiameter-bearingInnerEpsilon);
    translate([dia/2-1.5+xTransFlatExtra,-cuDim[1]/2,0])
      cube(cuDim);
  }
}
/*
cuDim = [bearingOuterDiameter,
           bearingOuterDiameter,
           bearingSocketZ];
projection()
translate([7.7/2-1.5-0.2+.4,-bearingOuterDiameter/2,0])
      cube(cuDim);
//color("blue",0.2)
//axisRaw(true);

*/

module potAxisCutter(){
  zTranslation = -potAxisLength + pedalWidth+8.0;
  rotate([0,0,180])
    translate([0,0,zTranslation])
      difference(){
        cylinder(h=potAxisLength, d= potAxisDiameter+potAxisEpsilon*4);
        translate([potAxisDiameter-potDOffSet+2*potAxisEpsilon,
                   -potAxisLength/2,
                   0])
          cube(potAxisLength);
      }
}
/*
color("green",0.6)
potAxisCutter(); 
color("red",0.1)
axisRaw(false);
*/

module axis(){
  difference(){
    axisRaw();
    potAxisCutter();
  }
}
//projection(cut=true)
//translate([0,0,-pedalWidth])
//axis();

module bearingSocketCutter(extended=false){
  height = (extended ? -(bearingSocketZ) + pedalWidth : bearingSocketZ);
  cylinder(h=height, //bearingSocketZ,
           d=bearingSocketD);
}
//bearingSocketCutter();

module bearingSocketCutters(extended=false){
  bearingSocketCutter(extended);
  translate([0,0,-(bearingSocketZ) + pedalWidth])
    bearingSocketCutter();
}

module pedalRaw(){
  x = pedalLength 
      + bearingInnerDiameter - bearingInnerEpsilon
      + pedalXExtender ;
  y = pedalDepth;
  z = pedalWidth;
  translate([-pedalXExtender ,-y/2,0])
    cube([x,y,z]);
}
//translate([0,0,-pedalWidth/2])
//pedalRaw();

module treadCutter(sens=1){
  depth = 0.5;
  length = pedalWidth/sin(45);
  translate([0,-pedalDepth/2,pedalWidth/2])
    rotate([0,-sens*45,0])
      cylinder(h=length,r=depth,center=true);
}

module treads(){
  for(x=[0:5:pedalLength 
        + bearingInnerDiameter - bearingInnerEpsilon
        + pedalXExtender])
        translate([x,0,0]){
      treadCutter();
      treadCutter(-1);
      }
}

module pedal(treads=true){
  difference(){
    union(){
      axis();
      difference(){
        pedalRaw();
        translate([0,pedalDepth,0])
          rotate([0,0,-7.5])
            scale([1.5,1,1])
              pedalRaw();
        bearingSocketCutters();
      }
    }
    if (treads)
      treads();
  }
}
//pedal();

module fullPedal(doPedal                = DO_PEDAL,
                 pedalId, // 0 none, 1, left, 2 right
                 gear                   = true,
                 Bearings               = true,
                 xtraWidthBearings      = false,
                 maximBearings          = false,
                 BearingCutters         = true,
                 ExtendedBearingCutters = false,
                 zAlign                 = 0 // 0: on Z==0; 1: centered on z=0; 2:gearbase on z=0
                 ){
  gearId = (pedalId == 1 ? 2 : 0);
  gearTra = [0,0,bearingSocketZ];
  zTra = (zAlign == 0 ? 0 :
                    (zAlign == 1 ? -pedalWidth/2 : -bearingSocketZ));                    
  tra = [0,0,zTra];
  mir = [0,0,(pedalId == 2 ? 1 : 0)];
  mirror(mir)
    translate(tra)
      union(){
        if (Bearings)
          if ($preview)
            color("red")
              bearings(xtraWidthBearings,maximBearings);
          else
            bearings(xtraWidthBearings,maximBearings);
        if (BearingCutters)
          if ($preview)
            color("blue",0.2)
              bearingSocketCutters(ExtendedBearingCutters);
          else
            bearingSocketCutters(ExtendedBearingCutters);
        if (doPedal) ///if(pedalId)
          if ($preview)
            color("green",0.2)
              pedal();  
          else  
            pedal(); 
        if(gear)
          translate(gearTra)
            mirror([0,0,1])
              if($preview)
                color("grey",0.2)
                  rotate([0,0,pedalNeutralPositionAlpha-90])
                    bevelGearPairRaw(together=false,gear_list=[0]);
              else 
                rotate([0,0,pedalNeutralPositionAlpha-90])
                  bevelGearPairRaw(together=false,gear_list=[0]);
      }        
}

module fullPedalAsselmbly(withGears=true,bearings=true,alignZ=true,extraWidthBearings=false,mxBear=false){
  fullTra = [0,0,(alignZ ? -getBevelAxisZ() : 0)];
  leftPedalTra = [0,0,2*getBevelAxisZ()];
  translate(fullTra)
    union(){
    if (withGears)
      if($preview)
        color("grey",0.2)
          rotate([0,0,pedalNeutralPositionAlpha-90])
            getGear(1,false);
      else 
        rotate([0,0,pedalNeutralPositionAlpha-90])
          getGear(1,false);
    fullPedal(pedalId=2,gear=withGears,Bearings=bearings,xtraWidthBearings=extraWidthBearings,
              maximBearings=mxBear,BearingCutters=false,zAlign=2);
    translate(leftPedalTra)
      fullPedal(pedalId=1,gear=withGears,Bearings=bearings,xtraWidthBearings=extraWidthBearings,
                maximBearings=mxBear,BearingCutters=false,zAlign=2);
    }
}
//fullPedalAsselmbly(false,false);

module assemblyPositioned(gears=false,upAngle=pedalNeutralPositionAlpha,xWidthBearings=false,maxBear=false){
  rotate([-90,-upAngle,0])
    fullPedalAsselmbly(withGears=gears,extraWidthBearings=xWidthBearings,mxBear=maxBear);
}
//rotate([90,0,0])
//assemblyPositioned(true);

cX = 100;
cY = cX;
cZ = 20;
thrustBearingZ       = 3.5; 
thrustBearingInnerD  = 3;
thrustBearingOuterD  = 8;
thrustBearingEpsilon = 0.2;
washerD              = 7+thrustBearingEpsilon;
washerDepth          = cZ/2;
boltDepth            = cZ;

// m3 data
boltD                = 3   + 0.2;
boltHeadDia          = 5.2 + 0.5;
boltHeadLength       = 3   + 0.2;
insertDia            = 4.3 + 0.2;
insertDepth          = 4   + 0.5;
boltAccessDia        = boltHeadDia;
nutAccessDia         = washerD;

module insertHoleWithBolt(boltPenetrationLength = 20,
                          boltProtusionLength   = 15,
                          totalProtusionLength  = 50,
                          bDia                  = boltD,
                          bHeadDia              = boltHeadDia,
                          accessDia             = boltAccessDia /*nutAccessDia*/,
                           ){
  // centered at (0,0), with positive Z starting at base of insert
  //insert
  cylinder(h=insertDepth,d=insertDia);
  // bolt
  translate([0,0,-boltProtusionLength])
    cylinder(h=boltPenetrationLength+boltProtusionLength,d=bDia);
  // bolt head
  translate([0,0,-(boltProtusionLength+boltHeadLength)])
    cylinder(h=boltHeadLength,d=boltHeadDia);
  //access hole
  accessHoleLength = totalProtusionLength - boltProtusionLength;
  accessHoleTopZ   = -(accessHoleLength + boltProtusionLength);
  translate([0,0,accessHoleTopZ])
    cylinder(h=accessHoleLength,d=accessDia);
  
}
//insertHoleWithBolt();

module insertHoleWithBoltLength15HeadDown(){
  insertHoleWithBolt(boltPenetrationLength = 10,
                     boltProtusionLength   = 5,
                     accessDia             = boltAccessDia);
}
//insertHoleWithBoltLength15HeadDown();  

mountingHoleX = 15.25;
mountingHoleY = 38.5669;
frontBlockZ = blockZ/2+coverZ;

module frontBaseMountingHoleCutters(){
  for (m= [0,1])
    mirror([0,m,0])
      translate([mountingHoleX,mountingHoleY,-(blockZ/2+coverZ)])
        insertHoleWithBoltLength15HeadDown();  
}
//%frontBaseMountingHoleCutters();

module bevelGearPairRaw(th=nbTeethPedalGear,
                        lth=nbTeethIdlerGear,
                        together=TOGETHER,
                        gear_list = [0,1,2],
                        doGearHull=DO_GEAR_HULL){
  difference(){
    if (doGearHull)
      hull()
        bevel_gear_pair(modul=1, gear_teeth=th, pinion_teeth=lth, axis_angle=90,
                        tooth_width=5, gear_bore=4, pinion_bore=idlerGearBore,
                        pressure_angle = 20, helix_angle=0, together_built=together,
                        gear_lis= gear_list);
    else
      bevel_gear_pair(modul=1, gear_teeth=th, pinion_teeth=lth, axis_angle=90,
                      tooth_width=5, gear_bore=4, pinion_bore=idlerGearBore,
                      pressure_angle = 20, helix_angle=0, together_built=together,
                      gear_lis= gear_list);
    intersection(){
      axisRaw(false);
      translate([0,0,pedalWidth])
        rotate([180,0,0])
          axisRaw(false);
    }
  }
}
//bevelGearPairRaw(together=false,gear_list=[0]);
//echo("call to get Z",getBevelAxisZ());

module getGear(g,position=true,doGearHull=DO_GEAR_HULL){
  zTra = (position ? -getBevelAxisZ() : 0);
  translate([0,0,zTra])
    if (doGearHull)
      hull()
        bevelGearPairRaw(gear_list=[g]);
    else
      bevelGearPairRaw(gear_list=[g]);
}
//projection()
//hull()
//color("blue",0.2)
//getGear(2);
//getGear(1);
//getGear(0);

module gearSpace(id,x=1,y=1,z=1){
  scale([x,y,z])
    hull()
      getGear(id,false);
}
//color("green",0.2)

function getBevelAxisZ(th=nbTeethPedalGear,
                       lth=nbTeethIdlerGear) = 
  get_bevel_gear_pair_axis_height(modul=1, gear_teeth=th, pinion_teeth=lth, axis_angle=90,
                                  tooth_width=5, gear_bore=4, pinion_bore=idlerGearBore,
                                  pressure_angle = 20, helix_angle=0, together_built=true);

module pignonAxel(eps=0){
  rotate([0,-90,0])
    cylinder(h=30,d=3+eps);
}
//pignonAxel();

module testPignonAxel(eps=.2){
  intersection(){
    getGear(1);
    pignonAxel(eps);
  }
}
//testPignonAxel();

