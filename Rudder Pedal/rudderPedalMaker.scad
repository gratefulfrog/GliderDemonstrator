$fn=100;


//////////////////// TARGETS //////////////////////

//all();
both();
//syncLever();
//doRudder(30);

////////////////////////////////////////////////////

// dimensions


bearingInnerDiameter = 8;
bearingOuterDiameter = 22;
bearingInnerEpsilon  = 0.2;
bearingOuterEpsilon  = 0.2;
bearingWidth         = 7;
bearingRimWidth      = 1.4;
bearingRimHeight     = 0.4;

bearingSocketwidth  = 6;

potAxisLength   = 15;
potDLength      = 7;
potAxisDiameter = 6;
potDOffSet      = 4.5;
potAxisEpsilon  = 0.2;

pedalWidth  = 30;
pedalDepth  = 15;
pedalLength = 100;
pedalXExtender = (bearingOuterDiameter+2*bearingSocketwidth)/2-2;

pedalInterSpace = 10;

insertDia   = 4.3 + 0.2;
insertDepth = 4   + 0.2;
boltDia     = 3   + 0.3;
boltDepth   = 20;

boltLength     = boltDepth + 15; // 20+15=35mm m3
boltTrueDia    = 3;
boltHeadHeight = 3;
boltHeadDia    = 5.25;

syncLeverX = 5;
syncLeverY = pedalDepth;
syncLeverZ = 2*pedalWidth+pedalInterSpace;

xDelta      = 2.5;

module doRudder(angle){
  rotate([angle,0,0]){
    syncLever();
  }
  rotate([0,0,angle]){
    all();
    bolt();
  }
  zTranslation = -(pedalWidth+pedalInterSpace);
  translate([0,0,zTranslation])
     rotate([0,0,-angle]){
      all();
      bolt();
    }
}
//doRudder(30);

module bolt(){
  translate([boltDepth-pedalXExtender-(boltLength+boltHeadHeight),
             0,
             (pedalWidth+pedalInterSpace)/2])
    rotate([0,-90,0]){
      translate([0,0,-boltHeadHeight])
        cylinder(h=boltHeadHeight,d=boltHeadDia);
      translate([0,0,-(boltLength+boltHeadHeight)])
        cylinder(h=boltLength,d=boltTrueDia);
    }
}
//rotate([0,0,30])
//bolt();

module bolter(angle,incs){
  angleInc = angle/incs;
  zInc =   getPhi(angle)/incs;//(pedalWidth/2+2*boltTrueDia-1)/incs;
  for (i= [0:1:incs]){
    translate([0,0,zInc*i/*-pedalWidth/2*/])
      rotate([0,0,angleInc*i])
        scale([1.7,1.7,1])
        bolt();
  }
}
module syncLever(maxAngle=30){
  xTranslation = syncLeverX/2 -(pedalXExtender + syncLeverX + xDelta);
  translate([xTranslation,0,0])
  difference(){
    syncLeverRaw();
    bolter(-maxAngle,100);
    bolter(maxAngle,100);
    mirror([0,0,1])
      union(){
        bolter(-maxAngle,100);
        bolter(maxAngle,100);
      }
  }
}
//syncLever();
//bolt();

function getPhi(alpha=30) = 
  let (maxAlpha = alpha,
       dd       = pedalXExtender + syncLeverX + 2.5 + syncLeverX,
       hh       = pedalWidth/2,
       rr       = dd*tan(maxAlpha),
       phi      = sqrt(rr*rr + hh*hh)-hh)
  phi;
//echo(getPhi());

module syncLeverRaw(){
  zTranslation = -syncLeverZ + pedalWidth+pedalInterSpace/2;
  yTranslation = -syncLeverY/2;
  xTranslation = -syncLeverX/2; //-(pedalXExtender + syncLeverX + xDelta);
  difference(){
    translate([xTranslation,yTranslation,zTranslation])
      cube([syncLeverX,syncLeverY,syncLeverZ]);
    rotate([0,90,0])
      cylinder(h=2*syncLeverX,d=boltDia,center=true);
    }
}
//syncLeverRaw();

module bearing(center=true){
  zTranslation = center ? -bearingWidth/2 : 0;
  translate([0,0,zTranslation]){
    difference(){
      cylinder(h=bearingWidth,d=bearingOuterDiameter);
      cylinder(h=bearingWidth,d=bearingOuterDiameter-2*bearingRimWidth);
    }
    difference(){
      cylinder(h=bearingWidth,d=bearingInnerDiameter+2*bearingRimWidth);
      cylinder(h=bearingWidth,d=bearingInnerDiameter);
    }
    translate([0,0,bearingRimHeight])
    difference(){
      cylinder(h=bearingWidth-2*bearingRimHeight,d=bearingOuterDiameter);
      cylinder(h=bearingWidth-2*bearingRimHeight,d=bearingInnerDiameter);
    }
  }
}
//bearing(false);

module bearings(){
  bearing(false);
  zTranslation = -bearingWidth + pedalWidth;
  translate([0,0,zTranslation])
    bearing(false);
}
/*
if($preview)
  color("red")
    bearings();
*/
  
module axisRaw(){
  cylinder(h=pedalWidth,d=bearingInnerDiameter-bearingInnerEpsilon);
}
//axisRaw();

module potAxisCutter(){
  zTranslation = -potAxisLength + pedalWidth;
  translate([0,0,zTranslation])
    difference(){
      cylinder(h=potAxisLength, d= potAxisDiameter+potAxisEpsilon);
      translate([potAxisDiameter-potDOffSet-potAxisEpsilon,-potAxisLength/2,0])
        cube(potAxisLength);
    }
}
//potAxisCutter(); 

module axis(){
  difference(){
    axisRaw();
    potAxisCutter();
  }
}
//axis();

module bearingSocketCutter(){
  cylinder(h=bearingWidth+0.5*bearingSocketwidth,
           d=bearingOuterDiameter+2*bearingSocketwidth);
}
//bearingSocketCutter();

module bearingSocketCutters(){
  bearingSocketCutter();
  translate([0,0,-(bearingWidth+0.5*bearingSocketwidth) + pedalWidth])
  bearingSocketCutter();
}
/*
if($preview)
  color("yellow", 0.3)
    bearingSocketCutters();
*/

module pedalRaw(){
  //xExtender = (bearingOuterDiameter+2*bearingSocketwidth)/2-2;
  x = pedalLength 
      + bearingInnerDiameter - bearingInnerEpsilon
      + pedalXExtender ;
  y = pedalDepth;
  z = pedalWidth;
  translate([-pedalXExtender ,-y/2,0])
    cube([x,y,z]);
}
/*
difference(){
pedalRaw();
translate([0,pedalDepth,0])
rotate([0,0,-5])
scale([1.5,1,1])
pedalRaw();
}
*/

module insertCutter(){
  translate([-pedalXExtender,0,pedalWidth/2])
    rotate([0,-90,0])
      rotate([180,0,0]){
        cylinder(h = insertDepth, d= insertDia);
        cylinder(h = boltDepth, d = boltDia);
      }
}
/*
if($preview)
  color("green",1)
    insertCutter();
*/

module pedal(){
  axis();
  difference(){
    pedalRaw();
    translate([0,pedalDepth,0])
      rotate([0,0,-6])
        scale([1.5,1,1])
        pedalRaw();
    bearingSocketCutters();
    insertCutter();
  }
}
//pedal();
/*
if($preview)
  color("blue",0.2)
    pedal();
else
  pedal();
*/

module all(){
  translate([0,0,pedalInterSpace/2])
  if($preview){
    color("red")
      bearings();
    color("yellow", 0.3)
      bearingSocketCutters();
    color("green",1)
      insertCutter();
    color("blue",0.2)
      pedal();
    color("orange",0.2)
      translate([0,0,-pedalInterSpace/2])
        bolt();
  }
  else {
    pedal();
    //translate([0,0,-pedalInterSpace/2])
      //bolt();
  }
}
//bolt();
//all();

module both(){
  zTranslation = -(pedalWidth+pedalInterSpace);
  all();
  translate([0,0,zTranslation])
    all();
  if($preview)
    color("blue",0.2)
      syncLever();
  else
    syncLever();
}
//both();
  