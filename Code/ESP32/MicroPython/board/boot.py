# This file is executed on every boot (including wake-boot from deepsleep)
#import esp
#esp.osdebug(None)

import network
import time
import webrepl

def do_connect():
    
    # WiFi SSID and Password
    wifi_ssid = ""             # YOUR WiFi SSID
    wifi_password = ""     # YOUR WiFi PASSWORD

    # Wireless config : Station mode
    station = network.WLAN(network.STA_IF)
    station.active(True)

    # Continually try to connect to WiFi access point
    while not station.isconnected():
    
        # Try to connect to WiFi access point
        print("Connecting...")
        station.connect(wifi_ssid, wifi_password)
        time.sleep(1)

    # Display connection details
    print("Connected!")
    print("My IP Address:", station.ifconfig()[0])
    

if __name__ == "__main__":
    do_connect()
    webrepl.start()

