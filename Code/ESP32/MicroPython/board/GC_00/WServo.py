#!/usr/bin/python3

from machine import Pin, PWM

def clamp(val,range):
    return min(max(val,range[0]),range[1])

def mapRaw(val,inRange,outRange):
    """ does proportional mapping with no checking of end points
    """
    ratio = (outRange[1]-outRange[0])/(inRange[1]-inRange[0])
    return outRange[0] + (val-inRange[0])*ratio

class WServo:
    def __init__(self,
                 pin,
                 minAlpha       = 45,
                 maxAlpha       = 135,
                 centerAlpha    = 90,
                 minPulse       = 800,
                 maxPulse       = 2200,
                 centerPulse    = 1500,
                 pulseFrequency = 50):
        self.pin          = pin
        self.alphaRange   = [minAlpha,maxAlpha]
        self.alphaCenter  = centerAlpha
        self.pulseRange   = [minPulse,maxPulse]
        self.pulseCenter  = centerPulse
        self.currentAlpha = None
        self.currentPulse = None
        self.pwm          = PWM(Pin(pin))
        self.pwm.freq(pulseFrequency)
        self.setAlpha(0)

    def __repr__(self):
        outstring = f'' + \
        f'Pin           : {self.pin}\n' + \
        f'Angular Range : {self.alphaRange}\n' + \
        f'Angle Center  : {self.alphaCenter}\n' + \
        f'Current Angle : {self.currentAlpha}\n' +\
        f'Pulse Range   : {self.pulseRange}\n' + \
        f'Current Pulse : {self.currentPulse}'
        return outstring

    def setAlpha(self,alpha,relative2Center = True):
        newAlpha = clamp(((alpha + self.alphaCenter) if relative2Center
                          else alpha),self.alphaRange)
        if (self.currentAlpha == newAlpha):
            return
        self.currentAlpha = newAlpha
        self.setPulse()
        print(f'Current Angle: {self.currentAlpha}')

    def setPulse(self):
        self.currentPulse = mapRaw(self.currentAlpha,self.alphaRange,self.pulseRange)
        self.pwm.duty_ns(int(1000*self.currentPulse))
        print(f'CurrentPulse: {self.currentPulse}')
        

def runTest():
    from time import sleep
    p=15
    s=WServo(p)
    print(s)
    for i in range(s.alphaRange[0],s.alphaRange[1]+1,10):
        s.setAlpha(i,False)
        sleep(.250)
        #print(s)
    for i in range(-45,45+1,10):
        s.setAlpha(i)
        sleep(.250)
        #print(s)
        
if __name__ == '__main__':
    runTest()
