#!/usr/bin/python3


class WindowFilter:
    def __init__(self,window,readFunc):
        self.window   = window
        self.readFunc = readFunc
        self.val      = self.readFunc()
        
    def read(self):
        reading = self.readFunc()
        self.val = reading if abs(reading-self.val)>self.window else self.val
        return self.val
            
