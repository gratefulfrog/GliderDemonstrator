#machine stubb

class Pin:
    def __init__(self,p):
        self.pin = p

class PWM:
    def __init__(self,p):
        self.p = p

    def freq(self,f):
        print(f'pwm.freq({f})')
        
    def duty_ns(self,n):
        print(f'pwm.duty_ns({n})')

class ADC:
    ATTN_11DB = 0
    
    def __init__(self,p):
        self.pin = p
        self.v = 0
        
    def atten(self,v):
        pass
    
    def read(self):
        self.v +=100
        print('ADC:',self.v)
        return self.v
        
    def read_u16(self):
        return self.read()
