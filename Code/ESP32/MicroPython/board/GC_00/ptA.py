# Complete project details at https://RandomNerdTutorials.com

from machine import Pin, ADC
from time import sleep
from WServo import WServo
from windowFilter import WindowFilter

"""
# PWM pins (servo): 0-19, 21-23, 25-27, 32-39
GPIOs ADC (pot):    0, 2, 4, 12, 13, 14, 15, 25, 
                   26, 27, 32, 33, 34, 35, 36, 39.
Resolution:        12-bit (0-4095)
Change resolution:  Yes
"""

# PWM pins: 0-19, 21-23, 25-27, 32-39
# ADC pins
  ## ADC1 (8 channels, attached to GPIOs 32 - 39)
  ## ADC2 (10 channels, attached to GPIOs 0, 2, 4, 12 - 15 and 25 - 27).

"""
*  Pins 1 and 3 are REPL UART TX and RX respectively
*  Pins 6, 7, 8, 11, 16, and 17 are used for connecting the embedded flash, 
   and are not recommended for other uses
*  Pins 34-39 are input only, and also do not have internal pull-up resistors

On the ESP32, ADC functionality is available on 
*  pins 32-39 (ADC block 1)
*  pins 0, 2, 4, 12-15 and 25-27 (ADC block 2).
ADC block 2 is also used by WiFi and so attempting to read analog values from block 2 pins when WiFi is active will raise an exception
"""
  

#IRange=[0,4095]  # read raw..
IRange=[0,65535]  # read raw..
ORange=[30,150]
Places = 1
Step = 0

def readPot(pin):
    pot = ADC(Pin(pin))
    pot.atten(ADC.ATTN_11DB)   #Full range: 3.3v
    while True:
        pot_value = pot.read()
        print(pot_value,mapR(pot_value))
        sleep(0.05)

def mapR(vIn,
         places,
         step,
         inRange=IRange,
         outRange=ORange):
    valIn = min(max(inRange[0],vIn),inRange[1])
    proportionIn = (valIn-min(inRange))/(inRange[1]-inRange[0])
    valOutRaw = min(outRange)+ proportionIn*(outRange[1]-outRange[0])
    return round2Step(valOutRaw,places,step) if step else round(valOutRaw,places)

def round2Step(v,places,step):
    temp = round(v,places)
    return temp-temp%step

def rtA():
    """shaky, but does something,
    need to check for nothing to do ...
    """
    potPin   = 32 
    pot = ADC(Pin(potPin))
    pot.atten(ADC.ATTN_11DB)   #Full range: 3.3v
    newReading = pot.read_u16()
    oldReading = -1
    while True:
        newReading = pot.read_u16()
        print('read_u16:',pot.read_u16()) #[0,65535]
        nb=10
        adcRaw = 0
        for i in range(nb):
            adcRaw+= pot.read_u16()
        avg= round(adcRaw/nb)
        print('avg:',avg,'delta avg/last:', abs(newReading-avg))
        sleep(1)

DB=500

def rtB():
    """moving window version
    """
    servoPin = 25 
    s = WServo(servoPin)
    potPin   = 32 
    pot = ADC(Pin(potPin))
    pot.atten(ADC.ATTN_11DB)   #Full range: 3.3v
    newReading = pot.read_u16()
    oldReading = newReading
    print('read_u16:',newReading) #[0,65535]
    newPotAngle = mapR(newReading,Places,Step)
    s.setAlpha(newPotAngle,False)
    while True:
        newReading = pot.read_u16()
        #print(newReading)
        #print('new:',newReading,'delta new/last:', abs(newReading-oldReading))
        if (abs(newReading-oldReading) >DB):
            print('read_u16:',newReading) #[0,65535]
            oldReading = newReading
            newPotAngle = mapR(newReading,1,0)
            print('newPotAngle:',newPotAngle)
            s.setAlpha(newPotAngle,False)
        sleep(sl)





"""
This works, but why?
* for the ADC, I set a 750 u16 window filter, so only readings separated by at
  least 750 will appear as new values
* I put a big buffer cap on the 5v supply to the servo!
Togther these 2 elements make it work!
"""

def rtC(db=750,p=1,ss=0):
    """window filter version
    """
    servoPin = 25 
    s = WServo(servoPin)
    potPin   = 32 
    pot = ADC(Pin(potPin))
    pot.atten(ADC.ATTN_11DB)   #Full range: 3.3v
    wf = WindowFilter(db,pot.read_u16)
    newReading = wf.read()
    oldReading = newReading
    print('wf.read',newReading) #[0,65535]
    newPotAngle = mapR(newReading,p,ss)
    s.setAlpha(newPotAngle,False)
    while True:
        newReading = wf.read()
        if newReading != oldReading:
            print('\nwf.read:',newReading) #[0,65535]
            oldReading = newReading
            newPotAngle = mapR(newReading,p,ss)
            print('newPotAngle:',newPotAngle)
            s.setAlpha(newPotAngle,False)
        sleep(sl)

sl = 0.1

def setAllServos(angle,boole,vec):
    for s in vec:
        s.setAlpha(angle,boole)

def rtD(db=750,p=1,ss=0):
    """window filter version
    running 4 servos at 5v more than 1.2a current draw !
    """
    servoPinVec = [25,26,27,14]
    servoVec =[]
    for p in servoPinVec:
        servoVec += [WServo(p)]
    #print(servoVec)
    #return
    potPin   = 32 
    pot = ADC(Pin(potPin))
    pot.atten(ADC.ATTN_11DB)   #Full range: 3.3v
    wf = WindowFilter(db,pot.read_u16)
    newReading = wf.read()
    oldReading = newReading
    print('wf.read',newReading) #[0,65535]
    newPotAngle = mapR(newReading,p,ss)
    #s.setAlpha(newPotAngle,False)
    setAllServos(newPotAngle,False,servoVec)
    while True:
        newReading = wf.read()
        if newReading != oldReading:
            print('\nwf.read:',newReading) #[0,65535]
            oldReading = newReading
            newPotAngle = mapR(newReading,p,ss)
            print('newPotAngle:',newPotAngle)
            setAllServos(newPotAngle,False,servoVec)
            #s.setAlpha(newPotAngle,False)
        sleep(sl)
