
from machine import Pin, ADC
from time import sleep
from WServo import WServo

"""
# PWM pins (servo): 0-19, 21-23, 25-27, 32-39
GPIOs ADC (pot):    0, 2, 4, 12, 13, 14, 15, 25, 
                   26, 27, 32, 33, 34, 35, 36, 39.
Resolution:        12-bit (0-4095)
Change resolution:  Yes
"""

# PWM pins: 0-19, 21-23, 25-27, 32-39
# ADC pins
  ## ADC1 (8 channels, attached to GPIOs 32 - 39)
  ## ADC2 (10 channels, attached to GPIOs 0, 2, 4, 12 - 15 and 25 - 27).

"""
*  Pins 1 and 3 are REPL UART TX and RX respectively
*  Pins 6, 7, 8, 11, 16, and 17 are used for connecting the embedded flash, 
   and are not recommended for other uses
*  Pins 34-39 are input only, and also do not have internal pull-up resistors

On the ESP32, ADC functionality is available on 
*  pins 32-39 (ADC block 1)
*  pins 0, 2, 4, 12-15 and 25-27 (ADC block 2).
ADC block 2 is also used by WiFi and so attempting to read analog values from block 2 pins when WiFi is active will raise an exception
"""
  

#IRange=[0,4095]  # read raw..
IRange=[0,65535]  # read raw..
ORange=[30,150]
Places = 0
Step = 5

def readPot(pin):
    pot = ADC(Pin(pin))
    pot.atten(ADC.ATTN_11DB)   #Full range: 3.3v
    while True:
        pot_value = pot.read()
        print(pot_value,mapR(pot_value))
        sleep(0.05)

def mapR(vIn,
         inRange=IRange,
         outRange=ORange,
         places=Places,
         step = Step):
    valIn = min(max(inRange[0],vIn),inRange[1])
    proportionIn = (valIn-min(inRange))/(inRange[1]-inRange[0])
    valOutRaw = min(outRange)+ proportionIn*(outRange[1]-outRange[0])
    return round2Step(valOutRaw,places,step)

def round2Step(v,places,step):
    temp = round(v,places)
    return temp-temp%step


def runTest():
    """shaky, but does something,
    need to check for nothing to do ...
    """
    servoPin = 25 
    potPin   = 32 
    s = WServo(servoPin)
    pot = ADC(Pin(potPin))
    pot.atten(ADC.ATTN_11DB)   #Full range: 3.3v
    newPotAngle = mapR(pot.read())
    oldPotAngle = -1
    while True:
        adcRaw = 0
        nb=50
        for i in range(nb):
            adcRaw+= pot.read_u16()
        newPotAngle = mapR(round(adcRaw/nb,-1))
        if (newPotAngle != oldPotAngle):
            print('pot reading in degress:',newPotAngle)
            #print('read_uv:',pot.read_uv())
            #print('read_u16:',pot.read_u16()) #[0,65535]
            s.setAlpha(newPotAngle,False)
            oldPotAngle = newPotAngle
        sleep(0.01)
    
