# Complete project details at https://RandomNerdTutorials.com

from machine import Pin, ADC
from time import sleep

"""
GPIOs:             0, 2, 4, 12, 13, 14, 15, 25, 
                   26, 27, 32, 33, 34, 35, 36, 39.
Resolution:        12-bit (0-4095)
Change resolution:  Yes
"""

IRange=[0,4095]
ORange=[0,180]
Places = 0
Step = 5

def readPot(pin):
    pot = ADC(Pin(pin))
    pot.atten(ADC.ATTN_11DB)   #Full range: 3.3v
    while True:
        pot_value = pot.read()
        print(pot_value,mapR(pot_value))
        sleep(0.5)

def mapR(vIn,
         inRange=IRange,
         outRange=ORange,
         places=Places,
         step = Step):
    valIn = min(max(inRange[0],vIn),inRange[1])
    proportionIn = (valIn-min(inRange))/(inRange[1]-inRange[0])
    valOutRaw = min(outRange)+ proportionIn*(outRange[1]-outRange[0])
    return round2Step(valOutRaw,places,step)

def round2Step(v,places,step):
    temp = round(v,places)
    return temp-temp%step

