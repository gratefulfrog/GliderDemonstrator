from servo import Servo
from time import sleep


# PWM pins: 0-19, 21-23, 25-27, 32-39
# ADC pins
  ## ADC1 (8 channels, attached to GPIOs 32 - 39)
  ## ADC2 (10 channels, attached to GPIOs 0, 2, 4, 12 - 15 and 25 - 27).


pinVec = [15,2,0,4]
maxUS = 1900
s = Servo(pin_id=22)
inc=30
def run(ms=s):
    global inc
    #inc=incr
    alpha=ms.read() #current_angle
    print('current angle:', alpha)
    if alpha>=180:
        inc = -abs(inc)
    elif alpha <=0:
        inc = abs(inc)
    alpha+=inc
    print('new angle:',alpha)
    ms.write(alpha)
    sleep(1)
