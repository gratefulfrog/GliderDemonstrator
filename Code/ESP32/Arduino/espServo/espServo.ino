#include <ESP32Servo.h>

Servo servo1 = Servo();

  //

void setup() {
  Serial.begin(115200);
  //servo1 = new Servo();
  servo1.attach(4, 500, 2500);
  // put your setup code here, to run once:

}

void loop() {
    for(int posDegrees = 0; posDegrees <= 180; posDegrees++) {
        servo1.write(posDegrees);
        Serial.println(posDegrees);
        delay(20);
    }

    for(int posDegrees = 180; posDegrees >= 0; posDegrees--) {
        servo1.write(posDegrees);
        Serial.println(posDegrees);
        delay(20);
    }
}