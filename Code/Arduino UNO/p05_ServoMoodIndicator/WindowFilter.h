#ifndef WINDOW_FILTER_H
#define WINDOW_FILTER_H

#include <Arduino.h>

typedef int (*readFuncPtr)();

class WindowFilter{
  private:
    int val,
        window;
    readFuncPtr readFunc;

  public:
    WindowFilter(int window, readFuncPtr rfp);
    int read();

};

#endif