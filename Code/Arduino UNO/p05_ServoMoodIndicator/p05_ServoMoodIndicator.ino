/*
  This works well.
  The sie of the window and the read frequency need to correspond. 
  A 100 window works well with 100ms read delay

  ma current with 4 servos 1.4A @5v
*/

// include the Servo library
#include <Servo.h>
#include "WindowFilter.h"

const int window    = 8,
          sleepTime = 10;

Servo servoVec[4];  // create a servo object

int potPin = A0;  // analog pin us10ed to connect the potentiometer
int potVal;             // variable to read the value from the analog pin
int angle,lastAngle;              // variable to hold the angle for the servo motor

int servoPinVec[] ={3,5,6,9};

int f(){
  return analogRead(potPin);
}

WindowFilter *wf ; //= WindowFilter(10,&f);

void setup() {
  Serial.begin(9600);  // open a serial connection to your computer
  while (!Serial) ;
  wf = new WindowFilter(window,&f);
  for (int i=0;i<4;i++){
    servoVec[i].attach(servoPinVec[i]);   // attaches the servo on pin 9 to the servo object
  }
}

void loop() {
  // read the value of the potentiometer
  potVal = wf->read();
  
  angle = map(potVal, 0, 1023, 30, 150);


  // set the servo position
  if (lastAngle!=angle){
    for (int i=0;i<4;i++){
      servoVec[i].write(angle);   // attaches the servo on pin 9 to the servo object
      // myservo.writeMicroseconds(1500)
      Serial.print("PotVal: ");
      Serial.print(potVal);
      Serial.print(",  Angle: ");
      Serial.println(angle);

    }
    lastAngle = angle;
  }

  // wait for the servo to get there
  delay(sleepTime);
}
