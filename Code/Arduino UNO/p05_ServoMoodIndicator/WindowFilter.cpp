#include "WindowFilter.h"

 WindowFilter::WindowFilter(int win, readFuncPtr rfp){
  window = win;
  readFunc = rfp;
  val = (*readFunc)();
}

int WindowFilter::read(){
  int reading = (*readFunc)();
  val = (abs(reading-val)>window ? reading : val);
  return val;
}