#ifndef CONFIG_H
#define CONFIG_H

#define AIL_R_CENTER_MS 1500;

class Config{
  public:
    static int forward =  1,
               reverse = -1;
               
    static enum servoID {ailR ,ailL,ele,rud};

};

#endif