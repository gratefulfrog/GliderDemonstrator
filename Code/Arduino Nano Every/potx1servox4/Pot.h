#ifndef POT_H
#define POT_H

#include <Arduino.h>
#inlcude "WindowFilter.h"

class Pot{
  private:
    int val,
        window;
    readFuncPtr readFunc;

  public:
    WindowFilter(int window, readFuncPtr rfp);
    int read();

};

#endif