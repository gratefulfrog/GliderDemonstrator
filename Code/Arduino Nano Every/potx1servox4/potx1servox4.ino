/*
  This works FAIRLY well.
  The size of the window and the read frequency need to correspond. 
  A 8 window works well with 100ms read delay

  max current with 4 servos 1.4A @5v !!
*/

#include <Servo.h>
#include "WindowFilter.h"

const int window    = 4,
          sleepTime = 100;

const int potPin   = A0,
          servoPinVec[] ={3,5,6,9},
          nbServos = 4,
          inRange[]  = {0, 1023},
          outRange[] = {30,150};

int potVal;       // variable to read the value from the analog pin
int lastAngle=-1; 

WindowFilter *wf ;   //= WindowFilter(10,&f);
Servo *servoVec[4];  

int readFunction(){
  return analogRead(potPin);
}

int mapR(int vIn,int *inRange,int *outRange){
  float proportion = (float)(vIn-inRange[0])/(inRange[1]-inRange[0]),
        outRaw     = min(outRange[1],max(outRange[0], outRange[0]+ proportion*(outRange[1]-outRange[0])));
  return round(outRaw);
}

void setup() {
  Serial.begin(9600);  // open a serial connection to your computer
  while (!Serial) ;
  wf = new WindowFilter(window,&readFunction);
  for (int i=0;i<nbServos;i++){
    servoVec[i] = new Servo();
    servoVec[i]->attach(servoPinVec[i]);   // attaches the servo on pin 9 to the servo object
  }
}

void loop() {
  // read the value of the potentiometer
  potVal = (int)wf->read();
  int angle = mapR(potVal,inRange,outRange); 

  // set the servo positionS
  if (angle!=lastAngle){
    for (int i=0;i<4;i++){
      servoVec[i]->write(angle);   // attaches the servo on pin 9 to the servo object
    }  
    Serial.print("PotVal: ");
    Serial.print(potVal);
    Serial.print(",  Angle: ");
    Serial.println(angle);
    lastAngle = angle;
  }
  // wait for the servo to get there
  delay(sleepTime);
}
