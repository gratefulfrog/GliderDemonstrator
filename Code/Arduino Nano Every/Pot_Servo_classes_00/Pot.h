#ifndef POT_H
#define POT_H

#include <Arduino.h>
#include "WindowFilter.h"
#include "Config.h"
#include "outils.h"

class Pot{
  private:
    WindowFilter *wf ;   
    int pin,
        range[2],
        centerVal;
    int _read() const;
  
  public:
    Pot(int pinIn,  int centerV = NULL, int *rangeIn=NULL); // center offset is in ADC units [0,1023]
    int read () const;
    int readOffset () const;  // offset is in ADC units [0,1023] from the centert val
    int *getRange() const;
    int  getCenter() const;
};

#endif