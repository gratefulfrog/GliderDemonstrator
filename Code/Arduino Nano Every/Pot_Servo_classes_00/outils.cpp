#include "outils.h"

int _mapR(int vIn,int *inRange,int *outRange){
  float proportion = (float)(vIn-inRange[0])/(inRange[1]-inRange[0]),
        outRaw     = min(outRange[1],max(outRange[0], outRange[0]+ proportion*(outRange[1]-outRange[0])));
  return round(outRaw);
}

int mapR(int vIn,int *inRange,int inCenter, int *outRange, int outCenter){
  int lowInRange[]   = {inRange[0],inCenter},
      highInRange[]  = {inCenter,inRange[1]},
      lowOutRange[]  = {outRange[0],outCenter},
      highOutRange[] = {outCenter,outRange[1]};
  
  if (vIn>inCenter){
    return _mapR(vIn,highInRange,highOutRange);
  }
  else{
    return _mapR(vIn,lowInRange,lowOutRange);
  }
}