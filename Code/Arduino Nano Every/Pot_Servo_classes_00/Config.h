#ifndef CONFIG_H
#define CONFIG_H

#define wDEBUG

/// Pot parameters
#define POT_PIN_RUDDER     (A3)
#define POT_PIN_ELE        (A1)
#define POT_PIN_AIL        (A2)

#define POT_MIN_VAL        (0)
#define POT_MAX_VAL        (1023)
#define POT_CENTER_VAL     (512)


/// Servo parameters
#define SERVO_PIN_RUDDER   (3)
#define SERVO_PIN_ELE      (5)
#define SERVO_PIN_AIL_L    (6)
#define SERVO_PIN_AIL_4    (9)

#define SERVO_MIN_US       (700)
#define SERVO_MAX_US       (2300)
#define SERVO_CENTER_US    (1500)

#define SERVO_MIN_ANGLE    (30)
#define SERVO_MAX_ANGLE    (150)
#define SERVO_CENTER_ANGLE (90)


#define  POT_WINDOW        (4)

#define  LOOP_DELAY        (100)  //ms

#endif