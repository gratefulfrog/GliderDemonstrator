/*
  This works FAIRLY well.
  The size of the window and the read frequency need to correspond. 
  A 8 window works well with 100ms read delay

  max current with 4 servos 1.4A @5v !!
*/

#include <Servo.h>
#include "Config.h"
//#include "WindowFilter.h"
#include "Pot.h"
#include "WServo.h"
#include "Control.h"
#include "outils.h"

const int servoPinVec[] ={3,5,6,9},
          nbServos = 4,
          inRange[]  = {0, 1023},
          outRange[] = {30,150};

int lastAngle=-1; 
Pot *potPtr;
Control *controlPtrVec[1];

// we create a single control which handles all 4 servos

void setup() {
  #ifdef wDEBUG
    Serial.begin(115200);  // open a serial connection to your computer
    while (!Serial) ;
  #endif
  potPtr = new Pot(POT_PIN_RUDDER,600); // second arg is center val
  
  // create a vector of pointers to pointers to WServos,
  WServo **pptr = new WServo*[nbServos];
  for (int i=0;i<nbServos;i++){
    // assign all the servos to the single control
    pptr[i] = new WServo(servoPinVec[i],i%2);
  }
  controlPtrVec[0] = new Control(potPtr,nbServos,pptr);
  
}

void loop() {
  for (int i=0;i<1;i++){
      #ifdef wDEBUG
        if(controlPtrVec[i]->update()){
          controlPtrVec[i]->show();
        }
      #else
        controlPtrVec[i]->update();
      #endif

    } 

  delay(LOOP_DELAY);
}
