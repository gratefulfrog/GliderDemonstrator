#ifndef WSERVO_H
#define WSERVO_H

#include <Arduino.h>
#include <Servo.h>
#include "Config.h"
#include "outils.h"

class WServo{
  private:
    Servo *servoPtr;
    int range[2],
        centerVal,
        invert;

  public:
    WServo();
    WServo(int pinIn, bool inverted = false,int *rangeIn=NULL, int centerV = NULL); // center offset is in degrees from center, range is degrees
    int *getRange() const;
    int  getCenter() const;
    void setAngle (int angle) const;
    void setAngleFromOffset (int offset) const;  // offset is in degrees from center
    int getAngle(bool asOffset=false) const; 
};

#endif