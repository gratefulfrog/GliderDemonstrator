#ifndef CONTROL_H
#define CONTROL_H

#include <Arduino.h>
#include "Pot.h"
#include "WServo.h"
#include "Config.h"
#include "outils.h"

// the Control Class maps a single pot to a vector of WServos.

class Control{
  private:
    Pot *potPtr;  // the Pot instance ptr that will command the servos
    WServo **wServoPtrVec;  // a vector of ptrs to WServos that will all be commanded by the Pot
    int lastPotOffset, 
        nbServos;
    
    int _mapIt(int newPotOffset,WServo* wServoPtr) const;
  
  public:
    Control(Pot *pp, int nbSers, WServo **wwSS); 
    bool update();
    int getPotOffsetVal() const;  // offset is in ADC units [0,1023] from the centert val
    int getServoOffSetAngle(int i) const; // get the servo offsety angle for the i'th WServo pointed in the vector of WServo ptrs.
    #ifdef wDEBUG
    void show() const;
    #endif
};

#endif