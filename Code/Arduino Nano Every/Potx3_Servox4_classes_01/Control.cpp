#include "Control.h"

//class Control{
  //private:


int Control::_mapIt(int newOffset, WServo* wServoPtr) const{
  // do the readings and get the corrected offset value for the servo
  return  mapR(newOffset+potPtr->getCenter(),
               potPtr->getRange(),
               potPtr->getCenter(),
               wServoPtr->getRange(),
               wServoPtr->getCenter());
}

Control::Control(Pot *pp, int nbSers, WServo **wwSS){ // center offset is in ADC units [0,1023]
  potPtr   = pp;
  nbServos = nbSers;
  #ifdef wDEBUG
      Serial.print("Control instance, nbSers: ");
      Serial.println(nbSers);
    #endif

  wServoPtrVec = new WServo*[nbServos];
  for (int i=0;i<nbServos;i++){
    wServoPtrVec[i] = wwSS[i];
  }
  lastPotOffset = -1;
}
bool Control::update(){
  int newPotOffset = potPtr->readOffset();
  if (newPotOffset != lastPotOffset){
    for (int i=0;i<nbServos;i++){
        wServoPtrVec[i]->setAngle( _mapIt(newPotOffset,wServoPtrVec[i]));
    }
    lastPotOffset = newPotOffset;
    return true;
  }
  return false;
}
int Control::getPotOffsetVal() const{  // offset is in ADC units [0,1023] from the center val
  return potPtr->readOffset ();
} 
int Control::getServoOffSetAngle(int i) const{
  return wServoPtrVec[i]->getAngle(true); 
}
#ifdef wDEBUG
void Control::show() const{
  Serial.print("Pot offset: ");
  Serial.println((int)potPtr->readOffset());
  for(int i=0;i<nbServos;i++){
    Serial.print("Servo: ");
    Serial.print(i);
    Serial.print(",  Servo offset angle: ");
    Serial.println(wServoPtrVec[i]->getAngle(true));
  }
}
#endif