#ifndef WSERVO_H
#define WSERVO_H

#include <Arduino.h>
#include <Servo.h>
#include "Config.h"
#include "outils.h"

/* This version compensates the filter window by stepping the servo degree by degree to 
 * the new target value, instead of jumping one window frame at a time.
 */

class WServo{
  private:
    Servo *servoPtr;
    int range[2],
        centerVal,
        invert;
    void stepTo(int targetAngle);
 
  public:
    WServo();
    WServo(int pinIn, bool inverted = false,int *rangeIn=NULL, int centerV = NULL); // center offset is in degrees from center, range is degrees
    int *getRange() const;
    int  getCenter() const;
    void setAngle (int angle) const;
    void setAngleFromOffset (int offset) const;  // offset is in degrees from center
    int getAngle(bool asOffset=false) const; 
};

#endif