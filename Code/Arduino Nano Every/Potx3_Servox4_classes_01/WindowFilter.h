#ifndef WINDOW_FILTER_H
#define WINDOW_FILTER_H

#include <Arduino.h>

//typedef int (*readFuncPtr)();

class WindowFilter{
  private:
    int pin,
        val,
        window;
    int _read() const;

  public:
    WindowFilter(int pin, int window);
    int read();

};

#endif