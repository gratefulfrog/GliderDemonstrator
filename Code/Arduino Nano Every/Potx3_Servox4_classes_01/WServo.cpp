#include "WServo.h"

WServo::WServo(){}
WServo::WServo(int pinIn,  bool inverted,int *rangeOut=NULL, int centerV){
  servoPtr = new Servo();
  #ifdef wDEBUG
      Serial.print("WServo instance for pin: ");
      Serial.println(pinIn);
  #endif
  servoPtr->attach(pinIn);
  if (rangeOut){
    for (int i=0;i<2;i++){
      range[i] = rangeOut[i];
    }
  }
  else{
    range[0] = SERVO_MIN_ANGLE;
    range[1] = SERVO_MAX_ANGLE;
  }
  centerVal  = centerV != NULL ? centerV : (range[0]+range[1])/2.;
  invert = inverted ? -1 : 1;
  servoPtr->write(centerVal);
} // center offset is in degrees from center
int *WServo::getRange() const{
  return range;
}
int  WServo::getCenter() const{
  return centerVal;
}
void WServo::setAngle (int angle) const{
  int angle2Set = invert*(angle-centerVal) + centerVal;
  //servoPtr->write(min(max(angle2Set,range[0]),range[1]));
  stepTo(min(max(angle2Set,range[0]),range[1]));
}
void WServo::setAngleFromOffset (int offset) const{  // offset is in degrees from center
  int angle = offset+centerVal;
  setAngle (angle);
} 
int WServo::getAngle(bool asOffset) const{
  int angle = servoPtr->read();
  return asOffset ? angle-centerVal : angle;
} 
void WServo::stepTo(int targetAngle) {
  int currentAngle = getAngle(false),
      sign = currentAngle > targetAngle ? -1 : 1;
  for (int step=currentAngle;step!=targetAngle; step+=sign)
    servoPtr->write(step);
}
