/*
  This works FAIRLY well.
  The size of the window and the read frequency need to correspond. 
  A 8 window works well with 100ms read delay

  max current with 4 servos 1.4A @5v !!
*/

#include <Servo.h>
#include "Config.h"
//#include "WindowFilter.h"
#include "Pot.h"
#include "WServo.h"
#include "Control.h"
#include "outils.h"

const int servoPinVec[]   = {SERVO_PIN_RUDDER, SERVO_PIN_ELE, SERVO_PIN_AIL_L,SERVO_PIN_AIL_R},
          potPinVec[]     = {POT_PIN_RUDDER,POT_PIN_ELE,POT_PIN_AIL},
          potServoNbVec[] = {1,1,2},  // 1 rudder servo, 1 ele servo, 2 ail servos
          nbPots          = 3,
          inRange[]       = {0, 1023},
          rudderInRange[]   = {512-126,512+98},
          outRange[]      = {30,150};

int lastAngle=-1; 

Control *controlPtrVec[nbPots];

// we create a single control which handles all 4 servos

void setup() {
  #ifdef wDEBUG
    Serial.begin(115200);  // open a serial connection to your computer
    while (!Serial) ;
    delay(1000);
    Serial.println("Starting up...");
    delay(1000);
  #endif
  int servoIndex = 0;
  for (int potIndex=0;potIndex<nbPots;potIndex++){
    
    // assign all the servos to the single control
    Pot *potPtr = (potIndex == 0 ?
                   new Pot(potPinVec[potIndex],-1,rudderInRange) :
                   new Pot(potPinVec[potIndex])) ;
    #ifdef wDEBUG
      Serial.print("servoIndex:");
      Serial.print(servoIndex);
      Serial.print(", potIndex:");
      Serial.print(potIndex);
      Serial.print(", potPtr==NULL:");
      Serial.println(potPtr==NULL);
    #endif
    WServo **sVecptr = new WServo*[potServoNbVec[potIndex]];
    #ifdef wDEBUG
      Serial.print("sVecptr==NULL:");
      Serial.println(sVecptr==NULL);
    #endif
    for (int j=0;j<potServoNbVec[potIndex];j++){
      //sVecptr[j] = new WServo(servoPinVec[servoIndex++],j%2);
      sVecptr[j] = new WServo(servoPinVec[servoIndex++],potIndex!=0);  // do not invert ailerons, invert servo 1 ELE
      #ifdef wDEBUG
        Serial.print("sVecptr[servoIndex-1]==NULL:");
        Serial.println(sVecptr[servoIndex-1]==NULL);
      #endif
    }
    #ifdef wDEBUG
      Serial.print("Creating Control instance:");
      Serial.print(potIndex);
      Serial.print(", Nb of servos in the instances:");
      Serial.println(potServoNbVec[potIndex]);
    #endif
    controlPtrVec[potIndex] = new Control(potPtr,potServoNbVec[potIndex],sVecptr);
  }
}

void loop() {
  for (int i=0;i<nbPots;i++){
      #ifdef wDEBUG
        if(controlPtrVec[i]->update()){
          Serial.print("Control instace: "),
          Serial.println(i),
          controlPtrVec[i]->show();
        }
      #else
        controlPtrVec[i]->update();
      #endif

    } 
  //Serial.println(analogRead(A0));
  delay(LOOP_DELAY);
}
