#include "WindowFilter.h"

 WindowFilter::WindowFilter(int pi, int win){
  pin = pi;
  window = win;
  val = _read();
}

int WindowFilter::_read() const{
  return analogRead(pin);
}

int WindowFilter::read(){
  int reading = _read();
  val = (abs(reading-val)>window ? reading : val);
  return val;
}