#include "Pot.h"

Pot::Pot(int pinIn, int centerV,int *rangeIn=NULL){// center value is in ADC units [0,1023]
  pin = pinIn;
  if (rangeIn){
    for (int i=0;i<2;i++){
      range[i] = rangeIn[i];
    }
  }
  else{
    range[0] = POT_MIN_VAL;
    range[1] = POT_MAX_VAL;
  }
  int trueCenter = round((range[0]+range[1])/2.);
  centerVal  = centerV != -1 ? centerV : trueCenter;
  wf = new WindowFilter(pin,POT_WINDOW);
}

int Pot::_read () const{
  return wf->read();
}
int * Pot::getRange() const{
  return range;
}
int  Pot::getCenter() const{
  return centerVal;
}
int Pot::read () const{
  return readOffset() + centerVal;
}

int Pot::readOffset () const{
  return _read()-centerVal;
}



