  $fn=500;

crossSectionDia = 8;
crossArcRad     = 13;
crossLength     = 34;
crossR          = 17;

m3Dia = 3.2;

insertHoleDia   = 4.4;
insertHoleDepth = 3.5;

bearingBaseDia   = 6.0;
bearingLipDia    = 7.18;
bearingDepth     = 2.5;
bearingLipDepth  = 0.6;
bearingBaseDepth = bearingDepth - bearingLipDepth;

bearingBaseSlop = 0.15;
bearingLipSlop  = 0.25;

bracketArmWidth  = 5;
bracketArmHeight = 10;
braketArmLength  = 43.6;
bracketBaseLength  = 45;
bracketArmBearingY = 35;

// cage
cageThickness    = 3;
cageRibThickness = 2;
cageRibHeight    = 3;
nbRibs           = 6;
ribSpacing       = 10.7;
cageX = 45.3 + 2*cageThickness;
cageY = (nbRibs-1)*ribSpacing + nbRibs*cageRibThickness; //64;
cageZ = 25 + cageThickness;

echo("cageY:",cageY);
///////////// TARGETS

//bracket();
//otherBracket();
//translate([0,bracketArmBearingY,0])
//crossAll();
cage();

//////////////////////

module cageTopRaw(){
  x = cageX;
  y = cageY;
  z = cageThickness ;
  cube([x,y,z],center = true);
}
//cageTopRaw();

module cageSideRaw(){
  x = cageZ;
  y = cageY;
  z = cageThickness ;
  cube([x,y,z],center = true);
}
//cageSideRaw();

module cageSide(left=true){
  rotY = left ? -90 : 90;
  transX = left ? -cageX/2+cageThickness/2 :  cageX/2+-cageThickness/2 ;
  translate([transX,0,-cageZ/2])
    rotate([0,rotY,0])
    union(){
      cageSideRaw();
      translate([0,-cageY/2+cageRibThickness/2,-cageRibHeight/2-cageThickness/2])
          ribs(cageZ);
    }
}
//cageSide();
//cageSide(false);

module cageRib(xLen,ribThicknessEpsilon=0){
  cube([xLen,(cageRibThickness-ribThicknessEpsilon),cageRibHeight],center = true);
}
//cageRib(cageX); 

module ribs(length,epsilon=0){
  for (i = [0:nbRibs-1])
    translate([0,i*(ribSpacing+cageRibThickness),0])
      cageRib(length,epsilon);
}
//ribs(cageX);

module cageTop(){
  translate([0,0,-cageThickness/2])
    union(){
      cageTopRaw();
      translate([0,-cageY/2+cageRibThickness/2,-cageRibHeight/2-cageThickness/2])
        ribs(cageX);
    }
}
//cageTop();

module cage(){
  rotate([0,180,0])
    union(){
      cageSide();
      cageSide(false);
      cageTop();
    }
}
//cage();

//circle(d=crossSectionDia);
module bearingHole (height){
  zTrans = bearingBaseDepth;
  cylinder(h=height,d=bearingBaseDia+bearingBaseSlop);
  translate([0,0,zTrans])
    cylinder(h=(height-zTrans),d=bearingLipDia+bearingLipSlop);
}
//bearingHole (10);

module bracketArm(){
  z = bracketArmWidth;
  x = bracketArmHeight;
  y = braketArmLength;
  cylDia = x;
  //cubeDim = x;
  difference(){
    translate([0,0,z/2])
      hull(){
        translate([0,z/2,0])
          cube([x,z,z],center = true);
        translate([0,y-x/2,0])
          cylinder(h=z,d=cylDia,center = true);
      }
    translate([0,bracketArmBearingY,0])
      bearingHole (z);
  }
}
//bracketArm();

module bracketArmPositioned(left=true){
  yRot   = left ? -90 : 90;
  xTrans = left ?  bracketArmWidth - bracketBaseLength/2 : -bracketArmWidth + bracketBaseLength/2 ;
  translate([xTrans,0,0])
    rotate([0,yRot,0])
      bracketArm();
}
//bracketArmPositioned();
//bracketArmPositioned(false);

module bracketBase(){
  x = bracketBaseLength;
  y = bracketArmWidth;
  z = bracketArmHeight;
  difference(){
    translate([0,y/2,0])
      cube([x,y,z],center = true);
    rotate([-90,0,0])
    bearingHole (5);
  }
}
//bracketBase();

module bracket(){
  bracketBase();
  bracketArmPositioned();
  bracketArmPositioned(false);  
}
//bracket();
//otherBracket();
//translate([0,bracketArmBearingY,0])
//crossAll();

module otherBracket(){
  translate([0,bracketArmBearingY,0])
    rotate([-90,0,0])
      translate([0,-bracketArmBearingY,0])
        rotate([0,90,0])
          bracket();
}
//otherBracket();

module cross(){
  xTrans = crossArcRad + crossSectionDia/2; 
  translate([-xTrans,-xTrans,0])
    rotate_extrude(angle=45)
      translate([xTrans,0,0])
        union(){
          circle(d=crossSectionDia);
          translate([crossSectionDia/4,0,0])
            square([crossSectionDia/2,crossSectionDia],center=true);
        }
}
//cross();

module crossX(){
  xTrans = crossArcRad + crossSectionDia/2; 
  translate([-xTrans,-xTrans,0])
    rotate_extrude(angle=23)
      translate([xTrans,0,0])
        difference(){
          translate([crossSectionDia/4,0,0])
            square([crossSectionDia/2,crossSectionDia],center=true);
//translate([0,bracketArmBearingY,0])
//crossAll();
          circle(d=crossSectionDia);
          
        }
}
//crossX();
module crossY(){
  difference(){
    cross();
    crossX();
    
  }
}
module crossZ(){
  crossY();
  rotate([0,0,-90])
  mirror([1,0,0])
  crossY();
}

module fullCross(){
  crossZ();
  mirror([1,0,0])
    crossZ();
  mirror([0,1,0])
    crossZ();
  mirror([1,1,0])
    crossZ();
}
module crossAll(){
  difference(){
    union(){
      fullCross();
      cylCross();
      cylinder(h=crossSectionDia,d= 10,center=true);
      }
  cylCross(m3Dia);
  insertHoles();
  }
}
//translate([0,bracketArmBearingY,0])
//crossAll();

module insertHole(){
  rotate([0,-90,0])
    cylinder(h=insertHoleDepth, d=insertHoleDia);
}
//insertHole();

module insertHoles(){
  nbHoles = 4;
  for (a = [0:nbHoles-1])
    rotate([0,0,a*90])
      translate([crossR,0,0])
        insertHole();
}
//insertHoles();translate([])
//circle(7.9);
//projection(cut=true)
//translate([0,0,-crossSectionDia/2 +0.1])

//cylinder(h=crossSectionDia,d= 18,center=true);

//cylinder(h=crossSectionDia,r=7.9,center=true);

//projection(cut=true)
//translate([0,0,-crossSectionDia/2 +0.1])
//translate([0,bracketArmBearingY,0])
//fullCross();
module cylCross(dd=crossSectionDia){
  rotate([90,0,0])
    cylinder(h=crossLength,d=dd,center=true);
  rotate([0,90,0])
    cylinder(h=crossLength,d=dd,center=true);
}
//cylCross();

